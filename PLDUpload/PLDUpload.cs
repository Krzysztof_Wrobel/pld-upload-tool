﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace PLDFileUpload
{
	internal class PLDUpload
	{
		private const string EOL = "\r\n";
		private const string BOUNDARY_NAME = "DATABOUNDARY";
		private const string BOUNDARY_MARK = "--" + BOUNDARY_NAME;
		private const string CONTENT_TYPE = "Content-type: ";
		private const string CONTENT_LENGTH = "Content-length: ";

		private readonly string _server;
		private readonly string _user;
		private readonly string _password;
		private readonly string _pserver;
		private readonly string _puser;
		private readonly string _ppassword;

		private string _moveDirectory;
		private string _logFile;

		public PLDUpload(string server, string user, string password, string pserver, string puser, string ppassword)
		{
			_server = server;
			_user = user;
			_password = password;
			_pserver = pserver;
			_puser = puser;
			_ppassword = ppassword;
		}

		private static void LogEntry(string s, bool writeToFile)
		{
			if (writeToFile)
				LogWriter.WriteToLogFile(s);
			else
				Console.WriteLine(s);
		}

		private static void LogException(string description, Exception ex, bool writeToFile)
		{
			if (writeToFile)
				LogWriter.WriteToLogFile(description, ex);
			else
			{
				LogEntry(description, false);
				LogEntry("Exception details:" + ex, false);
			}
		}

		private static string GetSegment(string aContentType, string aContentBody)
		{
			string segment = CONTENT_TYPE + aContentType + EOL;
			segment += CONTENT_LENGTH + aContentBody.Length + EOL;
			segment += EOL;
			segment += aContentBody + EOL;
			return segment;
		}

		private static string GetIdentificationSegment(string aUserId, string aPassword)
		{
			StringBuilder sb = new StringBuilder();
			sb.Append("AppVersion=1.0");
			sb.Append("&").Append("AcceptUPSLicenseAgreement=Yes");
			sb.Append("&").Append(@"ResponseType=application/x-ups-pld");
			sb.Append("&").Append("VersionNumber=V4R1");
			sb.Append("&").Append("UserId=").Append(aUserId);
			sb.Append("&").Append("Password=").Append(aPassword);
			sb.Append(EOL);

			return GetSegment(@"application/x-www-form-urlencoded", sb.ToString());
		}

		private static string GetDataSegment(string aData)
		{
			StringBuilder sb = new StringBuilder(aData);
			sb.Append(EOL);
			return GetSegment(@"application/x-ups-binary", sb.ToString());
		}

		private static byte[] GetEncodedString(string s)
		{
			return Encoding.ASCII.GetBytes(s);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="aFileName">PLD file path</param>
		/// <returns>0 - OK. File uploaded successfully and it is correct
		///1 - OK. File uploaded successfully. It has some error, but not fatal.
		///-1 - ERROR. Problem with file format or with transfering to server (e.g. communication error, request or response format)
		///-2 - ERROR. PLD file has fatal errors.
		///-3 - PLD file OK. Error during moving file.
		///</returns>
		private int UploadFile(string aFileName)
		{
			bool logToFile = !string.IsNullOrEmpty(_logFile);
			try
			{
				if (logToFile)
					LogWriter.OpenLogFile(_logFile);

				string fileContent;
				try
				{
					LogEntry("Reading contents of file " + aFileName + "...", logToFile);
					MemoryStream ms = new MemoryStream();
					using (FileStream input = File.OpenRead(aFileName))
					{
						byte[] buffer = new byte[1024];
						int count;
						while ((count = input.Read(buffer, 0, buffer.Length)) > 0)
							ms.Write(buffer, 0, count);
						fileContent = Encoding.ASCII.GetString(ms.ToArray());
					}
				}
				catch (Exception ex)
				{
					LogException("Reading file failed!", ex, logToFile);
					return -1;
				}

				byte[] bytes;
				try
				{
					LogEntry("Building request contents ...", logToFile);
					string message = BOUNDARY_MARK + EOL;
					message += GetIdentificationSegment(_user, _password);
					message += BOUNDARY_MARK + EOL;

					message += GetDataSegment(fileContent);
					message += BOUNDARY_MARK + EOL;

					bytes = GetEncodedString(message);
				}
				catch (Exception ex)
				{
					LogException("Building request contents failed!", ex, logToFile);
					return -1;
				}

				HttpWebRequest request;

				try
				{

					LogEntry("Creating request ...", logToFile);
					request = (HttpWebRequest)WebRequest.Create(_server);
					request.Method = "POST";
					request.ContentType = @"multipart/mixed; boundary=" + BOUNDARY_NAME;

					if (!string.IsNullOrEmpty(_pserver))
					{
						LogEntry("Creating proxy ...", logToFile);
						WebProxy proxy = new WebProxy(_pserver);
						if (!string.IsNullOrEmpty(_puser))
						{
							LogEntry("Creating proxy credentials ...", logToFile);
							proxy.Credentials = new NetworkCredential(_puser, _ppassword);
						}
						request.Proxy = proxy;
					}

					LogEntry("Writing data ...", logToFile);
					Stream reqStream = null;
					try
					{
						request.ContentLength = bytes.Length;
						reqStream = request.GetRequestStream();
						reqStream.Write(bytes, 0, bytes.Length);
					}
					finally
					{
						try
						{
							reqStream?.Close();
						}
						catch
						{
							// ignored
						}
					}
				}
				catch (Exception ex)
				{
					LogException("Creating request failed!", ex, logToFile);
					return -1;
				}

				string responseString;

				/*
				responseString = @"--BOUNDARY
				Content-type: text/html
				Content-length: 138

				<HTML>
				<HEAD>
				<TITLE>UPS Internet Software</TITLE>
				</HEAD>
				<BODY>
				<P>UPS Internet Software, Copyright UPS 1998</P>
				</BODY>
				</HTML>

				--BOUNDARY
				Content-type: application/x-ups-psmpld
				Content-length: 104

				UPSOnLine%1.0%6931%6503UserId is invalid.                                                          

				--BOUNDARY
				Content-type: application/x-ups-pld
				Content-length: 110

				00010000009710000008800026503UserId is invalid.                                                              

				--BOUNDARY--
				";
				*/

				LogEntry("Reading response ...", logToFile);
				HttpStatusCode status;

				string responseBoundary;
				try
				{
					using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
					{
						status = response.StatusCode;
						responseBoundary = "BOUNDARY";
						string boundaryRegex = @"^multipart/mixed;\s*boundary=(.*)$";
						Match match = Regex.Match(response.ContentType, boundaryRegex);
						if (match.Success && match.Groups.Count > 1)
						{
							responseBoundary = match.Groups[1].Value;
						}

						Stream s = response.GetResponseStream();
						using (MemoryStream responseStream = new MemoryStream())
						{
							byte[] buffer = new byte[1024];
							int read;
							while (s != null && (read = s.Read(buffer, 0, buffer.Length)) > 0)
								responseStream.Write(buffer, 0, read);

							responseString = Encoding.ASCII.GetString(responseStream.ToArray());
						}
						response.Close();
					}
				}
				catch (Exception ex)
				{
					LogException("Reading response failed!", ex, logToFile);
					return -1;
				}

				if (status != HttpStatusCode.OK)
				{
					LogEntry("Unexpected http status received: " + ((int)status) + " (" + status + ")", logToFile);
					LogEntry("Message contents:", logToFile);
					LogEntry(responseString, logToFile);
					return -1;
				}

				int result = -1;

				try
				{
					string resultRegex = @"^--{BOUNDARY}\s*^Content-type: application/x-ups-psmpld\s*^Content-length: \d+\s*UPSOnLine%1.0%(\d+)%(\d{4})(.*)".Replace("{BOUNDARY}", Regex.Escape(responseBoundary));

					Match match = Regex.Match(responseString, resultRegex, RegexOptions.Multiline);
					if (match.Success)
					{
						string code1 = match.Groups[1].Value;
						string code2 = match.Groups[2].Value;

						int completionCode = int.Parse(code1);
						int reasonCode = int.Parse(code2);
						if (completionCode == 0)
						{
							result = 0;
						}
						else if (completionCode == 6930 && reasonCode == 5481)
						{
							result = 1;
						}
						else
						{
							result = -2;
						}
					}
				}
				catch (Exception ex)
				{
					LogException("Evaluating of the response failed!", ex, logToFile);
				}

				if (_moveDirectory != null && (result == 0 || result == 1))
				{
					try
					{
						File.Move(aFileName, Path.Combine(_moveDirectory, Path.GetFileName(aFileName)));
					}
					catch (Exception ex)
					{
						result = -3;
						LogEntry($"File moving failed: {ex.Message}", logToFile);
					}
				}
				LogEntry("Message contents:", logToFile);
				LogEntry(responseString, logToFile);
				return result;
			}
			finally
			{
				if (logToFile)
					LogWriter.CloseLogFile();
			}
		}

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		/// <returns>0 - OK. File uploaded successfully and it is correct
		///1 - OK. File uploaded successfully. It has some error, but not fatal.
		///2 - Some files are OK, but some has Errors.
		///-1 - ERROR. Problem with file format or with transfering to server (e.g. communication error, request or response format)
		///-2 - ERROR. PLD file has fatal errors.
		///-3 - PLD file OK. Error during moving file.
		///	</returns>
		[STAThread]
		private static int Main(string[] args)
		{
			int result = 0;
			try
			{
				var tls12RequiredString = ConfigurationManager.AppSettings["Tls12Required"];

				LogEntry(string.Format("Read security protocol setting 'Tls12Required': {0}", tls12RequiredString), true);

				if (!string.IsNullOrEmpty(tls12RequiredString))
				{
					bool tls12Required;
					if (!bool.TryParse(tls12RequiredString, out tls12Required))
					{
						tls12Required = false;
					}
					else
					{
						LogEntry("Cannot convert security protocol setting!", true);
					}

					LogEntry(string.Format("Converted security protocol setting value: {0}", tls12Required), true);

					if (tls12Required)
					{
						ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
						LogEntry("Set security protocol to TLS 1.2", true);
					}
				}

				string server = null, user = null, password = null;
				string pserver = string.Empty, puser = string.Empty, ppassword = string.Empty;
				string pldFile = null, moveDirectory = null, logFile = null;

				string userRegex = @"^-u\{(.*?)\}";
				string passwordRegex = @"^-p\{(.*?)\}";
				string serverRegex = @"^-s\{(.*?)\}";
				string fileRegex = @"^-f\{(.*?)\}";
				string moveDirectoryRegex = @"^-m\{(.*?)\}";
				string logFileRegex = @"^-l\{(.*?)\}";

				string pServerRegex = @"^-ps\{(.*?)\}";
				string pUserRegex = @"^-pu\{(.*?)\}";
				string pPasswordRegex = @"^-pp\{(.*?)\}";

				foreach (string argument in args)
				{
					Match match;
					if (argument.ToLower().StartsWith("-ps"))
					{
						match = Regex.Match(argument, pServerRegex, RegexOptions.IgnoreCase);
						if (match.Success && match.Groups.Count > 1)
							pserver = match.Groups[1].Value;
						continue;
					}
					if (argument.ToLower().StartsWith("-pu"))
					{
						match = Regex.Match(argument, pUserRegex, RegexOptions.IgnoreCase);
						if (match.Success && match.Groups.Count > 1)
							puser = match.Groups[1].Value;
						continue;
					}
					if (argument.ToLower().StartsWith("-pp"))
					{
						match = Regex.Match(argument, pPasswordRegex, RegexOptions.IgnoreCase);
						if (match.Success && match.Groups.Count > 1)
							ppassword = match.Groups[1].Value;
						continue;
					}
					if (argument.ToLower().StartsWith("-u"))
					{
						match = Regex.Match(argument, userRegex, RegexOptions.IgnoreCase);
						if (match.Success && match.Groups.Count > 1)
							user = match.Groups[1].Value;
					}
					else if (argument.ToLower().StartsWith("-p"))
					{
						match = Regex.Match(argument, passwordRegex, RegexOptions.IgnoreCase);
						if (match.Success && match.Groups.Count > 1)
							password = match.Groups[1].Value;
					}
					else if (argument.ToLower().StartsWith("-s"))
					{
						match = Regex.Match(argument, serverRegex, RegexOptions.IgnoreCase);
						if (match.Success && match.Groups.Count > 1)
							server = match.Groups[1].Value;
					}
					else if (argument.ToLower().StartsWith("-f"))
					{
						match = Regex.Match(argument, fileRegex, RegexOptions.IgnoreCase);
						if (match.Success && match.Groups.Count > 1)
							pldFile = match.Groups[1].Value;
					}
					else if (argument.ToLower().StartsWith("-m"))
					{
						match = Regex.Match(argument, moveDirectoryRegex, RegexOptions.IgnoreCase);
						if (match.Success && match.Groups.Count > 1)
							moveDirectory = match.Groups[1].Value;
					}
					else if (argument.ToLower().StartsWith("-l"))
					{
						match = Regex.Match(argument, logFileRegex, RegexOptions.IgnoreCase);
						if (match.Success && match.Groups.Count > 1)
							logFile = match.Groups[1].Value;
					}
				}
				PLDUpload upload = new PLDUpload(server, user, password, pserver, puser, ppassword);
				upload._moveDirectory = moveDirectory;
				upload._logFile = logFile;
				string directory = Path.GetDirectoryName(pldFile);
				string file = Path.GetFileName(pldFile);
				string[] files = Directory.GetFiles(directory, file);
				int filesCount = files.Length;

				for (int i = 0; i < filesCount; i++)
				{
					file = files[i];
					int tmpResult = upload.UploadFile(file);
					if (i == 0)
						result = tmpResult;
					else
					{
						if (tmpResult == 0)
						{
							if (result < 0)
								result = 2;
						}
						else if (tmpResult == 1)
						{
							if (result == 0)
								result = 1;
							else if (result < 0)
								result = 2;
						}
						else if (tmpResult < 0)
						{
							if (result >= 0)
								result = 2;
						}
					}
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
			}
			return result;
		}
		//-s{server} -u{user} -p{password} -f{file} -m{directory} -l{logfile} -ps{proxyserver} -pu{proxyuser} pp{proxypassword}
	}
}