﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("PLD Upload Tool")]
[assembly: AssemblyDescription("PLD Upload Tool")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("MetaPack")]
[assembly: AssemblyProduct("PLD Upload Tool")]
[assembly: AssemblyCopyright("Copyright © MetaPack 2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("be5790de-b5fc-404c-9955-37519d4daff6")]

[assembly: AssemblyVersion("2.17.43.1")]
[assembly: AssemblyFileVersion("2.17.43.1")]