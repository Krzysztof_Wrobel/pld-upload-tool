﻿using System;
using System.IO;
using System.Text;

namespace PLDFileUpload
{
	public static class LogWriter
	{
		private static StreamWriter _logFile;

		public static void OpenLogFile(string logFilePath)
		{
			try
			{
				if (_logFile == null)
					_logFile = new StreamWriter(logFilePath, true, Encoding.Unicode);
			}
			catch
			{
				// ignored
			}
		}

		public static void WriteToLogFile(string message)
		{
			if (_logFile != null)
			{
				_logFile.WriteLine(DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString() + "  " + message);
				_logFile.Flush();
			}
		}

		public static void WriteToLogFile(string message, Exception ex)
		{
			WriteToLogFile(message);
			WriteToLogFile("Exception details:" + ex);
		}

		public static void CloseLogFile()
		{
			try
			{
				_logFile?.Close();
				_logFile = null;
			}
			catch
			{
				// ignored
			}
		}
	}
}